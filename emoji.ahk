emoji := false
#M::
	emoji := !emoji
	if (emoji) {
		MsgBox Emoji On
	}else {
		MsgBox Emoji Off
	}
return

:*C:a::
	if (emoji) {
		Send, 🅰️{Space}
	} else {
		Send, a
	}
return

:*C:b::
	if (emoji) {
		Send, 🅱️{Space}
	} else {
		Send, b
	}
return

:*C:c::
	if (emoji) {
		Send, 🇨{Space}
	} else {
		Send, c
	}
return

:*C:d::
	if (emoji) {
		Send, 🇩{Space}
	} else {
		Send, d
	}
return

:*C:e::
	if (emoji) {
		Send, 🇪{Space}
	} else {
		Send, e
	}
return

:*C:f::
	if (emoji) {
		Send, 🇫{Space}
	} else {
		Send, f
	}
return

:*C:g::
	if (emoji) {
		Send, 🇬{Space}
	} else {
		Send, g
	}
return

:*C:h::
	if (emoji) {
		Send, 🇭{Space}
	} else {
		Send, h
	}
return

:*C:i::
	if (emoji) {
		Send, 🇮{Space}
	} else {
		Send, i
	}
return

:*C:j::
	if (emoji) {
		Send, 🇯{Space}
	} else {
		Send, j
	}
return

:*C:k::
	if (emoji) {
		Send, 🇰{Space}
	} else {
		Send, k
	}
return

:*C:l::
	if (emoji) {
		Send, 🇱{Space}
	} else {
		Send, l
	}
return

:*C:m::
	if (emoji) {
		Send, 🇲{Space}
	} else {
		Send, m
	}
return

:*C:n::
	if (emoji) {
		Send, 🇳{Space}
	} else {
		Send, n
	}
return

:*C:o::
	if (emoji) {
		Send, 🇴{Space}
	} else {
		Send, o
	}
return

:*C:p::
	if (emoji) {
		Send, 🅿️{Space}
	} else {
		Send, p
	}
return

:*C:q::
	if (emoji) {
		Send, 🇶{Space}
	} else {
		Send, q
	}
return

:*C:r::
	if (emoji) {
		Send, 🇷{Space}
	} else {
		Send, r
	}
return

:*C:s::
	if (emoji) {
		Send, 🇸{Space}
	} else {
		Send, s
	}
return

:*C:t::
	if (emoji) {
		Send, 🇹{Space}
	} else {
		Send, t
	}
return

:*C:u::
	if (emoji) {
		Send, 🇺{Space}
	} else {
		Send, u
	}
return

:*C:v::
	if (emoji) {
		Send, 🇻{Space}
	} else {
		Send, v
	}
return

:*C:w::
	if (emoji) {
		Send, 🇼{Space}
	} else {
		Send, w
	}
return

:*C:x::
	if (emoji) {
		Send, 🇽{Space}
	} else {
		Send, x
	}
return

:*C:y::
	if (emoji) {
		Send, 🇾{Space}
	} else {
		Send, y
	}
return

:*C:z::
	if (emoji) {
		Send, 🇿{Space}
	} else {
		Send, z
	}
return
